package com.techo.repository;

import org.springframework.data.repository.CrudRepository;

import com.techo.domain.surveys.Survey;


/**
 * @author alan.hryniewicz
 *
 */
public interface SurveyRepository extends CrudRepository<Survey, Integer>{

}
