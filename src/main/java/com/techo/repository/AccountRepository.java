package com.techo.repository;

import org.springframework.data.repository.CrudRepository;

import com.techo.domain.security.Account;

public interface AccountRepository extends CrudRepository<Account, Integer>{

	public Account findByUserName(String userName);
	
}
