package com.techo.repository;

import org.springframework.data.repository.CrudRepository;

import com.techo.domain.families.Family;

public interface FamilyRepository extends CrudRepository<Family, Integer>{

}
