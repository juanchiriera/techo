package com.techo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author alan.hryniewicz
 *
 * Runs application on the embedded tomcat.
 */
@SpringBootApplication
public class TechoApplication {

	public static void main(String[] args) {
		SpringApplication.run(TechoApplication.class, args);
	}
}
