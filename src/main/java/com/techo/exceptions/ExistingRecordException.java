package com.techo.exceptions;

public class ExistingRecordException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2133920634170041612L;

	public ExistingRecordException(String message) {
		super("Trying to add existing record with id: " + message);
	}

}