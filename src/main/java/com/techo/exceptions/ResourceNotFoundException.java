package com.techo.exceptions;

public class ResourceNotFoundException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6222842591994183889L;

	public ResourceNotFoundException(String message) {
		super("Resource not found for: " + message);
	}

}
