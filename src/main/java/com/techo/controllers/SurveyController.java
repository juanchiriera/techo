package com.techo.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.techo.domain.surveys.Survey;
import com.techo.services.SurveyService;

/**
 * @author alan.hryniewicz
 *
 *         Controller for surveys.
 */
@RestController
@RequestMapping("/form")
public class SurveyController {

	@Autowired
	SurveyService surveyService;

	@GetMapping
	public List<Survey> getAll() {
		return surveyService.getAll();
	}

	@GetMapping("/{id}")
	public Survey get(@PathVariable Integer id) {
		return surveyService.get(id);
	}

	@RequestMapping(method = RequestMethod.POST)
	public void add(@RequestBody Survey form) {
		surveyService.add(form);
	}
}
