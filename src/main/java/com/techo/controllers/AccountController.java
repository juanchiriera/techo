package com.techo.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.techo.domain.security.Account;
import com.techo.services.AccountService;

/**
 * @author alan.hryniewicz
 *
 */
@RestController
@RequestMapping("/account")
public class AccountController {

	@Autowired
	private AccountService accountService;

	@RequestMapping(method = RequestMethod.GET)
	public List<Account> getAll() {
		return accountService.getAll();
	}

	@RequestMapping(method = RequestMethod.POST)
	public void create(@RequestBody Account account) {
		accountService.add(account);
	}

	@RequestMapping(method = RequestMethod.DELETE, value = "/{id}")
	public void delete(@PathVariable Integer id) {
		accountService.hardDelete(id);
	}

	@RequestMapping(method = RequestMethod.PUT, value = "/{id}")
	public void update(@PathVariable Integer id, @RequestBody Account account) {
		accountService.update(id, account);
	}

}
