package com.techo.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class MainController {

    @RequestMapping("/")
    public String index1() {
        return "/index";
    }

    @RequestMapping("/index")
    public String index() {
        return "index";
    }
    
    @RequestMapping("/hello")
    public String hello(Model model) {
        model.addAttribute("name", "Tom");
        model.addAttribute("formatted", "<b>blue</b>");
        return "hello";
    }

    @GetMapping("/admin")
    public String admin() {
        return "/admin";
    }

    @GetMapping("/user")
    public String user() {
        return "/user";
    }

    @GetMapping("/about")
    public String about() {
        return "/about";
    }

    @GetMapping("/login")
    public String login() {
        return "/login";
    }

    @GetMapping("/403")
    public String error403() {
        return "/error/403";
    }
}
