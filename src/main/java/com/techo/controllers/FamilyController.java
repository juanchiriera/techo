package com.techo.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.techo.domain.families.Family;
import com.techo.services.FamilyService;

/**
 * @author alan.hryniewicz
 *
 *         Controller for families.
 */
@RestController
@RequestMapping("/family")
public class FamilyController {

	@Autowired
	FamilyService familyService;

	@GetMapping
	public List<Family> getAll() {
		return familyService.getAll();
	}

	@GetMapping("/{id}")
	public Family get(@PathVariable Integer id) {
		return familyService.get(id);
	}

	@RequestMapping(method = RequestMethod.POST)
	public void add(@RequestBody Family family) {
		familyService.add(family);
	}
}