package com.techo.domain.construction;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.OneToMany;

/**
 * @author alan.hryniewicz
 *
 * A construction is a date in which one or many buildings are built
 * in one or many neighborhoods.
 */
@Entity
public class Construction {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int id;
	@Column
	private Date date;
	@Column
	private String name;
	@OneToMany
	@JoinTable(name="constructionbyneighborhood", joinColumns={
			@JoinColumn(referencedColumnName="id", name="construction")
	},inverseJoinColumns={@JoinColumn(referencedColumnName="id", name="neighborhood")}
	)
	private List<Neighborhood> neighborhoods;
	
	public Construction() {
		this.neighborhoods = new ArrayList<Neighborhood>();
	}
	
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public List<Neighborhood> getNeighborhoods() {
		return neighborhoods;
	}

	public void setNeighborhoods(List<Neighborhood> neighborhoods) {
		this.neighborhoods = neighborhoods;
	}

}
