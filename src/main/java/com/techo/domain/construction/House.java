package com.techo.domain.construction;

import javax.persistence.DiscriminatorValue;

/**
 * @author alan.hryniewicz
 *
 */
@DiscriminatorValue(value="house")
public class House extends Building {
	
	public House(){}
	
}
