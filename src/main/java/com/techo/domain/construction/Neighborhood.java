package com.techo.domain.construction;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;

/**
 * @author alan.hryniewicz
 *
 */
@Entity
public class Neighborhood {

	@Id
	@GeneratedValue
	private int id;
	@Column
	private String name;
	@OneToMany(mappedBy="neighborhood")
	private List<Building> buildings;

	public Neighborhood() {
		this.buildings = new ArrayList<Building>();
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public List<Building> getBuildings() {
		return buildings;
	}

	public void setBuildings(List<Building> buildings) {
		this.buildings = buildings;
	}

}
