package com.techo.domain.construction;

import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.techo.domain.families.Family;

/**
 * @author alan.hryniewicz
 *
 * Abstract class to extend to potential classes of buildings,
 * right now we just have houses, but in the future we may have
 * departments, PHs, etc.
 */
@Entity
@Table(name="building")
@Inheritance(strategy=InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name="type")
public abstract class Building {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;
	@OneToOne
	private Family family;
	@Column
	private String couponNumber;
	@ManyToOne
	@JoinColumn(name="neighborhood", referencedColumnName="id")
	private Neighborhood neighborhood;
	
	public Building() {}

	public Family getFamily() {
		return family;
	}

	public void setFamily(Family family) {
		this.family = family;
	}

	public int getId() {
		return id;
	}

	public String getCouponNumber() {
		return couponNumber;
	}

	public void setCouponNumber(String couponNumber) {
		this.couponNumber = couponNumber;
	}

}
