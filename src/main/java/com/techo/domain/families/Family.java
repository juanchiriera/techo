package com.techo.domain.families;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import com.techo.domain.Identifiable;
import com.techo.domain.surveys.Survey;

/**
 * @author alan.hryniewicz
 *
 */
@Entity
public class Family implements Identifiable<Integer> {

	@Id
	@GeneratedValue
	private Integer id;
	private String adress;
	@OneToMany(cascade = CascadeType.ALL)
	private List<FamilyBoss> familyBosses;
	@OneToOne(cascade = CascadeType.ALL)
	private Survey survey;

	public Family() {
		this.familyBosses = new ArrayList<FamilyBoss>();
	}

	public String getAdress() {
		return adress;
	}

	public void setAdress(String adress) {
		this.adress = adress;
	}

	public List<FamilyBoss> getFamilyBosses() {
		return familyBosses;
	}

	public void setFamilyBosses(List<FamilyBoss> familyBosses) {
		this.familyBosses = familyBosses;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Survey getSurvey() {
		return survey;
	}

	public void setSurvey(Survey survey) {
		this.survey = survey;
	}

}
