package com.techo.domain.families.visits;

/**
 * @author alan.hryniewicz
 *
 * States of priority we handle, this are being set by interviewers on
 * each visit, in a near future the ponderation will help us to do an
 * algorithm to assign a building to a family.
 */
public enum Priority {

	ALTA("Alta",1),
	MEDIA_ALTA("Media alta",2),
	MEDIA_BAJA("Media baja",3),
	BAJA("Baja",4);
	
	private String priority;
	private int ponderation;

	Priority(String priority, int ponderation) {
		this.priority = priority;
		this.ponderation = ponderation;
	}

	public String getPriority() {
		return priority;
	}

	public int getPonderation() {
		return ponderation;
	}
	
}
