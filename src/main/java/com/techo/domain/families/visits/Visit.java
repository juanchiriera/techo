package com.techo.domain.families.visits;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

/**
 * @author alan.hryniewicz
 *
 */
@Entity
public class Visit {

	@Id
	@GeneratedValue
	private int id;
	@Column(name = "date")
	private Date date;
	@Column(name = "priority")
	@Enumerated(EnumType.STRING)
	private Priority surveyedPriority;

	public Visit() {
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public Priority getSurveyedPriority() {
		return surveyedPriority;
	}

	public void setSurveyedPriority(Priority surveyedPriority) {
		this.surveyedPriority = surveyedPriority;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

}
