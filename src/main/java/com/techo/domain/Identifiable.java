package com.techo.domain;

public interface Identifiable<PK extends Number> {

	PK getId();
	void setId(Integer id);
}
