package com.techo.domain.security;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

import com.techo.domain.Identifiable;
import com.techo.domain.volunteers.Volunteer;

/**
 * @author alan.hryniewicz
 *
 */
@Entity(name = "account")
public class Account implements Identifiable<Integer> {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;
	@Column(name = "username")
	private String userName;
	@Column(name = "password")
	private String password;
	@Column(name="enabled")
	private Integer enabled = 1;
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "volunteer")
	private Volunteer volunteer;

	public Account() {
	}

	public Account(String userName, String password) {
		this.userName = userName;
		this.password = password;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Volunteer getVolunteer() {
		return volunteer;
	}

	public void setVolunteer(Volunteer volunteer) {
		this.volunteer = volunteer;
	}

	public Integer getEnabled() {
		return enabled;
	}

	public void setEnabled(Integer enabled) {
		this.enabled = enabled;
	}

}