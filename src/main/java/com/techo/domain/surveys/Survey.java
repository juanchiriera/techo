package com.techo.domain.surveys;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import com.techo.domain.Identifiable;
import com.techo.domain.assignations.Status;
import com.techo.domain.construction.Neighborhood;
import com.techo.domain.families.visits.Visit;

/**
 * @author alan.hryniewicz
 *
 */
@Entity
public class Survey implements Identifiable<Integer> {

	@Id
	@GeneratedValue
	@Column(name = "id")
	private Integer id;
	@OneToOne(cascade = CascadeType.ALL)
	private Neighborhood neighborhood;
	@OneToMany(cascade = CascadeType.ALL)
	private List<Visit> visits;
	@Column(name = "status")
	@Enumerated(EnumType.STRING)
	private Status status;

	public Survey() {
		this.visits = new ArrayList<Visit>();
		this.status = Status.EN_CURSO;
	}

	public List<Visit> getVisits() {
		return visits;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Neighborhood getNeighborhood() {
		return neighborhood;
	}

	public void setNeighborhood(Neighborhood neighborhood) {
		this.neighborhood = neighborhood;
	}

	public void setVisits(List<Visit> visits) {
		this.visits = visits;
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

}
