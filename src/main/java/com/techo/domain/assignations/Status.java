package com.techo.domain.assignations;


/**
 * @author alan.hryniewicz
 *
 * Possible statuses for an assignation.
 */
public enum Status {
	
	CONSTRUIDO("Construido"),
	PEDIDO_PRIMER_VISITA("Pedido primer visita"),
	EN_CURSO("En curso"),
	VER_MAS_ADELANTE("Ver mas adelante"),
	CERRADO("Cerrado"),
	ASIGNADA("Asignada"),
	ASIGNADA_CON_SUBSIDIO("Asignada con subsidio"),
	DESASIGNADA("Desasignada");
	
	private String status;
	
	Status (String status) {
		this.status = status;
	}

	public String getStatus() {
		return status;
	}
		
}
