package com.techo.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.techo.domain.families.Family;
import com.techo.repository.FamilyRepository;

/**
 * @author alan.hryniewicz
 *
 */
@Service
@Transactional
public class FamilyService implements BasicService<Integer, Family> {

	@Autowired
	private FamilyRepository familyRepository;

	@Override
	public CrudRepository<Family, Integer> getRepository() {
		return familyRepository;
	}

}
