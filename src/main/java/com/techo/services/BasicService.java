package com.techo.services;

import java.util.ArrayList;
import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.techo.domain.Identifiable;
import com.techo.exceptions.ExistingRecordException;
import com.techo.exceptions.ResourceNotFoundException;

interface BasicService<PK extends Number, U extends Identifiable<PK>> {

	default List<U> getAll() {
		List<U> objects = new ArrayList<U>();
		getRepository().findAll().forEach(o -> objects.add(o));
		return objects;
	}

	default U get(PK id) {
		U object = getRepository().findOne(id);
		if (object == null) {
			throw new ResourceNotFoundException(id.toString());
		}
		return object;
	}

	default void hardDelete(PK id) {
		getRepository().delete(id);
	}

	default void add(U object) {
		if (object.getId() != null && getRepository().exists(object.getId())) {
			throw new ExistingRecordException(object.getId().toString());
		}
		getRepository().save(object);
	}

	CrudRepository<U, PK> getRepository();
}
