package com.techo.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.techo.domain.surveys.Survey;
import com.techo.repository.SurveyRepository;

/**
 * @author alan.hryniewicz
 *
 */
@Service
@Transactional
public class SurveyService implements BasicService<Integer, Survey> {

	@Autowired
	private SurveyRepository surveyRepository;

	@Override
	public CrudRepository<Survey, Integer> getRepository() {
		return surveyRepository;
	}

}
