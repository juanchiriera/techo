package com.techo.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.techo.domain.security.Account;
import com.techo.domain.volunteers.Volunteer;
import com.techo.repository.AccountRepository;

/**
 * @author alan.hryniewicz
 *
 */
@Service
@Transactional
public class AccountService implements BasicService<Integer, Account> {

	@Autowired
	private AccountRepository accountRepository;

	public void update(Integer id, Account newAccount) {
		Account oldAccount = accountRepository.findOne(id);
		oldAccount.setPassword(newAccount.getPassword());
		oldAccount.setUserName(newAccount.getUserName());
		Volunteer volunteer = oldAccount.getVolunteer();
		Volunteer updatedVolunteer = newAccount.getVolunteer();
		volunteer.setAddress(updatedVolunteer.getAddress());
		volunteer.setEmail(updatedVolunteer.getEmail());
		volunteer.setFirstName(updatedVolunteer.getFirstName());
		volunteer.setLastName(updatedVolunteer.getLastName());
		volunteer.setPhoneNumber(updatedVolunteer.getPhoneNumber());
		accountRepository.save(oldAccount);
	}

	@Override
	public CrudRepository<Account, Integer> getRepository() {
		return accountRepository;
	}
}
